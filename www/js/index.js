var app = {
  initialize: function() {
    this.bindEvents();
  },
  bindEvents: function() {
    document.addEventListener('deviceready', this.onDeviceReady, false);
  },
  onDeviceReady: function() {
    app.receivedEvent('deviceready');

    var paymentForm = document.getElementById('paymentForm');
    paymentForm.onsubmit = function (e) {
      e.preventDefault();

      var paymentUrlText = document.getElementById('paymentURL');
      paymentUrlText = paymentUrlText.value.trim();

      var postData = document.getElementById('postParams');
      postData = postData.value.trim();

      var reachedEndUrl = function () {
        var button = document.getElementById('submitButton');
        button.innerHTML = 'End URL Reached!';
        button.classList.add('green');
        button.classList.remove('blue');

        window.setTimeout(function () {
          button.innerHTML = 'Go';
          button.classList.remove('green');
          button.classList.add('blue');
        }, 3000);
      };

      var abortedCallback = function () {
        var button = document.getElementById('submitButton');
        button.innerHTML = 'User Aborted';
        button.classList.add('red');
        button.classList.remove('blue');

        window.setTimeout(function () {
          button.innerHTML = 'Go';
          button.classList.remove('red');
          button.classList.add('blue');
        }, 3000);
      };

      var myBrowserParams = {
        "url": paymentUrlText,
        "merchantId": "godel_qa",
        "clientId": "android",
        "transactionId": "123",
        "orderId": "12",
        "amount": "1000",
        "customerEmail": "godel-qa@juspay.in",
        "postData": postData
      };

      JuspaySafeBrowser.start({
        endUrls: ['.*google.*'],
        onEndUrlReached: reachedEndUrl,
        onTransactionAborted: abortedCallback,
        browserParams: myBrowserParams
      });
    };
  },
  receivedEvent: function(id) {
  }
};

app.initialize();
