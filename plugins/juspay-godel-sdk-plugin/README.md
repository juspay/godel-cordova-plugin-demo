# PhoneGap app integration

JusPay has a native android client which can be used by PhoneGap applications. To get started, you first need to download the code for the plugin.

## Installation
This requires phonegap/cordova CLI 5.0+ (current stable v1.5.3).

Install via repo url directly (latest version)
```sh
phonegap plugin add https://bitbucket.org/juspay/godel-cordova-plugin
```
or
```sh
cordova plugin add https://bitbucket.org/juspay/godel-cordova-plugin
```
## Android Details

### Permissions

The plugin will automatically make the following entries into the `AndroidManifest.xml` of
your hybrid application.

```xml
<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="android.permission.READ_SMS" />
<uses-permission android:name="android.permission.RECEIVE_SMS" />
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
<uses-permission android:name="android.permission.READ_PHONE_STATE"/>
```

The `in.juspay.godel.PaymentActivity` is also added to `<application>`:

```xml
<activity
     android:label="Processing your payment"
     android:name="in.juspay.godel.PaymentActivity"
     android:hardwareAccelerated="true"
     android:screenOrientation="portrait"
     android:windowSoftInputMode="adjustResize"
   >
 </activity>
```


## Starting payment flow

### Define Return Url Callback
When the user has completed the payment (could be success or failure), the user will be redirected to the `return_url` configured by you at the time of order creation. JusPay will invoke your function when the user has reached this `return_url`.
```javascript
var reachedEndUrl = function () {
    // your code to check the server status
    var paymentStatus = getPaymentStatusFromServer()
    if(paymentStatus == "CHARGED") {
        gotoThankYouPage()
    }
    else {
        gotoFailurePage()
    }
};
```
**Note**: JusPay will not provide the payment status in the callback function, because it is a security issue. You will have to check with your server and retrieve the status from JusPay using `/order/status` API.

### Define back button callback
If the user presses back button, then the transaction is aborted midway by the user. Our plugin will let you know when this happens through a callback. You may define the function as:
```javascript
var abortedCallback = function () {
    gotoFailurePage()
};
```
The plugin will handle all the payment pages and when the user has completed the payment, the user is finally redirected to your website. To stop the plugin at the correct end URL, you must declare the final return URL that you have configured with JusPay.
```javascript
var myEndUrls = ["https://shop.com/juspay-response/.*", "https://beta.shop.com/juspay-response/.*"]
```

### Specify the browserParams

The `browserParams` object is used to pass all the required information that must be
passed to the JusPay safe browser.

Find below the set of parameters which must be sent:

 | Variable | Description | Type| Required | Default |
 |----------|------------|-----------|-----------|----|
 | url | Start URL for payment | String | Yes | |
 | postData | POST parameters that must be passed to the URL | String | No | |
 | merchantId	| Identifies the merchant. Eg. 'merchant’ | String | Yes |  |
 | clientId |	ClientID defines the platform used. Eg. 'merchant_android’ | String |	Yes | |
 | transactionId |	Represents the current transactionId | String |	Yes | |
 | orderId | Represents the order number assigned by the merchant | String | Yes | |
 | amount | Amount of the transaction | String | Yes | |
 | customerId | Unique identifier of the customer | String | No | |
 | customerEmail | Email address of the customer | String | Yes | |
 | customerPhoneNumber | Mobile number of the customer | String | No | |
 | displayNote | Short note about transaction shown to the customer. ex. 'Paying INR 200 for Order 123456' | String | No | |
 | remarks | Remarks about transaction. This will be automatically filled up in the netbanking page. ex. 'Pay to merchant' | String | No | |
 | clearCookies | If true, we will clear webView cookies whenever Juspay Webview is initialized. Set argument to false if you dont want to clear cookies | Boolean | No | true |

Sample `browserParams` :

```javascript
var myBrowserParams = {
    "url": "https://sandbox.juspay.in/merchant/ipay?order_id=1474096418&merchant_id=sriduth_sandbox_test&payment_options=card|nb|wallet&mobile=true",
    "merchantId": "sriduth_test",
    "clientId": "android",
    "transactionId": "123",
    "orderId": "12",
    "amount": "1000",
    "customerEmail": "sriduth.jayhari@gmail.com"
}
```

### Customizing the Action Bar

Juspay safe browser alllows the user to customize the action bar. To customize the acton bar, pass in a JSON configuration to the `ExpressCheckout.startCheckoutActivity` method as shown:

```javascript
var myActionBar = {
    "backgroundColor": "#FF0000",
    "backgroundImage": "my-drawable-resource",
    "icon": "my-drawable-resource",
    "displayNote": "Flight Mumbai <-> Delhi",
    "remarks": "Flight MUM DEL"
};
```

| Option | Description |
| ------ | ----------- |
| backgroundColor | Sets the background color of the action bar, this must be a hex color code, like `#f9c7d1` |
| backgroundImage | Name of the file to be used as backgroundImage of action bar. Note that if this property and `backgroundColor` are set, `backgroundImage` will override `backgroundColor` |
| icon | Name of the file to be set as the icon image. |
| displayNote | Short note about transaction shown to the customer. ex. 'Paying INR 200 for Order 123456' |
| remarks | Remarks about transaction. This will be automatically filled up in the netbanking page. ex. 'Pay to merchant' |

**Note**: The images to be used for `icon` and `backgroundImage` must be placed in the drawables resource folder of android. See this [link](https://cordova.apache.org/docs/en/latest/config_ref/images.html#configuring-icons-in-the-cli) for more information.

### Wrapping all together

Once the required parameters are prepared, call `JuspaySafeBrowser.start` to load
the url specified in the browserParams.

```javascript
var reachedEndUrl = function () {
    // your code to check the server status
    var paymentStatus = getPaymentStatusFromServer()
    if(paymentStatus == "CHARGED") {
        gotoThankYouPage()
    }
    else {
        gotoFailurePage()
    }
};

var abortedCallback = function () {
    gotoFailurePage()
};

var myBrowserParams = {
    "url": "https://sandbox.juspay.in/merchant/ipay?order_id=1474096418&merchant_id=sriduth_sandbox_test&payment_options=card|nb|wallet&mobile=true",
    "merchantId": "sriduth_test",
    "clientId": "android",
    "transactionId": "123",
    "orderId": "12",
    "amount": "1000",
    "customerEmail": "sriduth.jayhari@gmail.com"
}

var myActionBar = {
    "backgroundColor": "#FF0000",
    "backgroundImage": "my-drawable-resource",
    "icon": "my-drawable-resource",
    "displayNote": "Flight Mumbai <-> Delhi",
    "remarks": "Flight MUM DEL"
};

JuspaySafeBrowser.start({
    endUrls: myEndUrls,
    onEndUrlReached: reachedEndUrl,
    onTransactionAborted: abortedCallback,
    browserParams: myBrowserParams,
    actionBar: myActionBar
})
```

## Examples

We have setup simple examples for you to see the code working end to end. Examples are provided for both Cordova & PhoneGap. Check the working examples [here](https://bitbucket.org/juspay/godel-cordova-plugin/src/HEAD/examples/?at=master).

## Help & Support

If you notice any errors or issues with the integration, please reach out to us at support@juspay.in. You may also search our [Knowledge base](http://faq.juspay.in) to see if the issue has already been addressed by our team.
