//
//  JPSafeBrowser.m
//  
//
//  Created by Balaganesh S on 01/06/17.
//
//

#import "JPSafeBrowser.h"
#import <JuspaySafeBrowser/JuspaySafeBrowser.h>

@interface JPSafeBrowser()

@property (nonatomic, strong) JuspaySafeBrowser *browser;

@end

@implementation JPSafeBrowser

- (void)dismissView {
    [self.browser backButtonPressed];
}

- (void)JuspayStartPayment:(CDVInvokedUrlCommand*)command {
    
    NSString* callbackId = command.callbackId;
    NSDictionary *arguments = [[command arguments] objectAtIndex:0];
    NSDictionary *params = [arguments objectForKey:@"browserParams"];
    
    self.browser = [[JuspaySafeBrowser alloc] init];
    
    BrowserParams *browserParams = [[BrowserParams alloc] init];
    browserParams.merchantId = [params objectForKey:@"merchantId"];
    browserParams.clientId = [params objectForKey:@"clientId"];
    browserParams.url = [params objectForKey:@"url"];
    browserParams.transactionId = [params objectForKey:@"transactionId"];
    browserParams.amount = [params objectForKey:@"amount"];
    browserParams.customerEmail = [params objectForKey:@"customerEmail"];
    browserParams.endUrlRegexes = [arguments objectForKey:@"endUrlRegexes"];
    
    UIViewController *juspayViewController = [[UIViewController alloc] init];
    CGRect frame = self.viewController.view.frame;
    frame.size.height -= 20;
    juspayViewController.view.frame = frame;
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(dismissView)];
    juspayViewController.navigationItem.leftBarButtonItem = item;
    juspayViewController.edgesForExtendedLayout = UIRectEdgeNone;
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:juspayViewController];
    [navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor]}];
    navigationController.navigationBar.translucent = NO;
    
    [self.viewController presentViewController:navigationController animated:YES completion:nil];
    
    [self.browser startpaymentWithJuspayInView:juspayViewController.view withParameters:browserParams callback:^(Boolean status, NSError * _Nullable error, id  _Nullable info) {
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        if (error) {
            [dict setObject:error.description forKey:@"error"];
        }
        [dict setObject:[NSNumber numberWithBool:status] forKey:@"status"];
        [dict setObject:info forKey:@"info"];
        
        
        CDVPluginResult* result = [CDVPluginResult
                                   resultWithStatus:CDVCommandStatus_OK
                                   messageAsDictionary:dict];
        [self.commandDelegate sendPluginResult:result callbackId:callbackId];
    }];
    
}

@end
