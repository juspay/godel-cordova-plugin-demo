/**
 * Main module that packs the payment functionality and success / failure callbacks
 */
var godel = {
  ABORTED: 'txn-aborted'
};

Object.defineProperty(godel, 'start', {
  enumerable: false,
  value: function(configs) {
    var requiredFields = [{
      key: 'endUrls',
      validator: function (urls) {
        return 'length' in urls && urls.length > 0    ;
      }
    }, {
      key: 'onEndUrlReached',
      validator: function (f) {
        return typeof f === 'function';
      }
    }, {
      key: 'onTransactionAborted',
      validator: function (f) {
        return typeof f === 'function';
      }
    }, {
      key: 'browserParams',
      validator : function (params) {
        console.log(params);
        var browserParamsRequiredFields = [
          'url',
          'merchantId',
          'clientId',
          'transactionId',
          'orderId',
          'amount',
          'customerEmail'
        ];

        var take = true;
        for(var i=0; i<browserParamsRequiredFields.length; i+=1) {
          take = take && (params.hasOwnProperty(browserParamsRequiredFields[i]) &&
                          params[browserParamsRequiredFields[i]].length > 0);
        }

        return take;
      }
    }];

    for(var e in requiredFields) {
      var field = requiredFields[e];
      if(!(field.key in configs) || !field.validator(configs[field.key])) {
        throw "Error: Please specify the following field: " + field.key;
      }
    }

    var options = [{
      'browserParams': configs.browserParams,
      'endUrlRegexes': configs.endUrls,
      'actionBar': configs.actionBar
    }];

    var successDispatcher = function(args) {
      switch (args._method) {
      case 'endUrlReached':
        configs.endUrlReached(args.data);
        break;
      case 'onTransactionAborted':
        configs.onTransactionAborted(args.data);
        break;
      }
    };

    var errorDispatcher = function(args) {
      configs.error();
    };

    cordova.exec(
      successDispatcher,
      errorDispatcher,
      "JuspaySafeBrowser",
      "JuspayStartPayment",
      options
    );
  }
});

if('freeze' in Object && typeof Object.freeze === 'function') {
  godel = Object.freeze(godel);
}

module.exports = godel;
